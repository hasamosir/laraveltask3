<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'item' => $this->faker->words(2, true),
            'description' => $this->faker->words(3, true),
            'qty' => $this->faker->randomNumber(2, true),
            'price' => $this->faker->randomNumber(5, true),
            'image_url' => '',
        ];
    }
}
