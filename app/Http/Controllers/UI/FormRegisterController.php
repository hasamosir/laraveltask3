<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class FormRegisterController extends Controller
{
    public function showForm()
    {
        return view('admin.register.add');
    }

    public function register(Request $request)
    {
        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => $request->password,
        ];
        $response = $this->getResponseFromApi('http://grouptask.test/api/register', $data);
        if ($response === false) {
            return abort(500);
        }

        $response = json_decode($response);
        return view('admin.register.view-otp', compact('response'));
    }

    public function verify(Request $request)
    {
        $data = [
            'phone' => $request->phone,
            'otp' => $request->otp,
        ];
        $response = $this->getResponseFromApi('http://grouptask.test/api/verify-otp', $data);
        if ($response === false) {
            return abort(500);
        }

        $response = json_decode($response);
        return view('admin.register.response', compact('response'));
    }

    private function getResponseFromApi($endPoint, $data = null)
    {
        try {
            $response = Http::asForm()->post($endPoint, $data);
            return $response->getBody();
        } catch (\Throwable $err) {
            Log::error($err);
            return false;
        }
    }
}
