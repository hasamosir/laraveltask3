<?php

namespace App\Http\Controllers\UI;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class LoginController extends Controller
{
    public function create()
    {
        return view('admin.login.home');
    }

    public function store(Request $request)
    {
        $data = [
            'phone' => $request->phone,
            'password' => $request->password,
        ];
        $status = $this->getResponseFromApi('http://grouptask.test/api/login', $data);
        if ($status === false) {
            return abort(500);
        }

        $status = json_decode($status);
        return view('admin.login.view-otp', compact('status'));
    }

    public function verifyLogin(Request $request)
    {
        $data = [
            // 'phone' => $request->phone,
            'otp' => $request->otp,
        ];
        var_dump($status = $this->getResponseFromApi('http://grouptask.test/admin/verify-login', $data));

        if ($status === false) {
            return abort(500);
        }

        $status = json_decode($status);
        return view('admin.login.response', compact('status'));
    }

    private function getResponseFromApi($endPoint, $data = null)
    {
        try {
            $status = Http::asForm()->post($endPoint, $data);
            return $status->getBody();
        } catch (\Throwable $err) {
            Log::error($err);
            return false;
        }
    }
}
