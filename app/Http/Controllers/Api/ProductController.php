<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $product = Product::all();
        // return response(json_encode($product));
        // dd($product[0]->images);
        return response()->json($product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $result = $request->file('image')->store('images');

        // $images = Image::where('url', $request->url);
        // dd($images);

        // if ($images) {

        // }
        // $result = $request->file('image')->store('images');
        // $result = Image::where('url', $request->url);
        // if ($result) {

        //     $product = new Product();
        //     $product->image_url = $result;
        // }

        // $images = Image::where('url', $request->url);
        // if ($images->hasFile('url')->isValid()) {
        //     //
        // }

        // $result = new Image();
        // $result->url = $request->url;

        // return response()->json($result);

        // if ($result) {
        //     dd($result);

        //     $product = new Product();
        //     $product->image_url = $result;
        //     $imgProduct = $product->image_url;
        // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $result = $request->file('image')->store('images');
        // return $result;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
