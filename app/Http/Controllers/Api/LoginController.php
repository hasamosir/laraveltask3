<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Otp;
use App\Models\Product;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $status = [
            'status' => 'not authorized',
        ];

        $phone = $request->phone;

        $existAccess = User::where('phone', $phone)->first();
        if ($existAccess) {
            $credential = ['id' => $existAccess->id, 'password' => $request->password];
            if (Auth::attempt($credential) === true) {
                $token = $existAccess->createToken('authToken');

                $otp = $this->generateOtp($existAccess->id);

                $status = [
                    'status' => 'success',
                    'user' => auth()->user(),
                    'otp' => $otp,
                    'token' => $token->plainTextToken
                ];
                return response(json_encode($status), 200, ['Content-Type' => 'application/json']);
            }
        }
        return response(json_encode($status), 401, ['Content-Type' => 'application/json']);
    }
    private function generateOtp($user_id)
    {
        /** generate 2 digit, 3kali generate */
        $code = rand(11, 99);
        $code = $code . rand(11, 99);
        $code = $code . rand(11, 99);

        $otp = new Otp();
        $otp->user_id = $user_id;
        $otp->code = $code;
        $otp->save();

        return $otp->code;
    }

    public function verifyOtp(Request $request)
    {
        /** bandingin ke database */
        $existAccess = User::where('phone', $request->phone)->first();
        if ($existAccess) {
            // dd($existAccess->otps()->orderBy('id', 'DESC')->first());
            $otp = $existAccess->otps()->orderBy('id', 'DESC')->first();
            $otpCode = $otp->code;

            if ($otpCode == $request->otp) {

                return response()->json([
                    'status' => 'ok',
                    'message' => 'verification success'
                ]);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'false'
                ]);
            }
        }
    }

    public function product()
    {
        $product = Product::all();
        return response()->json($product);
    }
}
