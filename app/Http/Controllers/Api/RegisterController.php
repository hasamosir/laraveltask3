<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Otp;

class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $existAccess = User::where('email', $request->email)->orWhere('phone', $request->phone)->first();
        if ($existAccess) {
            /** jika user ada */
            return response()->json([
                'status' => 'error',
                'message' => 'user is exist'
            ]);
        } else {
            /** jika ga ada */
            $access = new User();
            $access->name = $request->name;
            $access->email = $request->email;
            $access->phone = $request->phone;
            $access->password = password_hash($request->password, PASSWORD_BCRYPT);

            $access->save();

            /** generate otp */
            $otp = $this->generateOtp($access->id);

            return response()->json([
                'status' => 'ok',
                'data' => [
                    'phone' => $request->phone,
                    'otp' => $otp
                ],
                'message' => 'succsess'
            ]);
        }
    }

    private function generateOtp($user_id)
    {
        /** generate 2 digit, 3kali generate */
        $code = rand(11, 99);
        $code = $code . rand(11, 99);
        $code = $code . rand(11, 99);

        $otp = new Otp();
        $otp->user_id = $user_id;
        $otp->code = $code;
        $otp->save();

        return $otp->code;
    }

    public function verifyOtp(Request $request)
    {
        /** bandingin ke database */
        $existAccess = User::where('phone', $request->phone)->first();
        if ($existAccess) {
            // dd($existAccess->otps()->orderBy('id', 'DESC')->first());
            $otp = $existAccess->otps()->orderBy('id', 'DESC')->first();
            $otpCode = $otp->code;

            if ($otpCode == $request->otp) {
                $existAccess->phone_verified_at = date('Y-m-d h:i:s');
                $existAccess->save();

                return response()->json([
                    'status' => 'ok',
                    'message' => 'verification success'
                ]);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'false'
                ]);
            }
        }
    }
}
