<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Voucher;
use Illuminate\Http\Request;

class VoucherController extends Controller
{
    public function voucher()
    {
        $diskon = 10000;
        $minTrans = 50000;
        // $vouchers = Voucher::where('discount', $diskon)
        $vouchers = Voucher::create([
            'status' => 'error',
            'date_start' => date('Y-m-d'),
            'date_end' => "2022-06-01",
            'min_trans_amount' => $minTrans,
            'discount' => $diskon

        ]);

        dd($vouchers);
    }
}
