<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\CartDetail;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class BookedController extends Controller
{
    public function checkout(Request $request)
    {
        $user = auth()->user();
        if (!$user) {
            return response()->json([
                'status' => 'error',
                'message' => 'unauthorized'
            ]);
        }

        $product_id = $request->product_id;
        $qty = $request->qty;

        $cart = Cart::where('user_id', $user->id)->first();
        if (!$cart) {
            $cart = new Cart();
            $cart->user_id = $user->id;
        }
        $details = CartDetail::select('product_id', DB::raw('sum(qty) as qty'))->where('cart_id', $cart->id)->where('product_id', $product_id)->groupBy('product_id')->first();
        $detailsQty = 0;
        if (isset($details->qty)) {
            $detailsQty = $details->qty;
        }

        $product = Product::find($product_id);
        if (!$product) {
            return response()->json([
                'status' => 'error',
                'message' => 'not found'
            ]);
        } else {
            if (($qty + $detailsQty) > $product->qty) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'out of stock'
                ]);
            }
        }

        if (!$cart->save()) {
            return response()->json([
                'status' => 'error',
                'message' => 'server error'
            ]);
        }

        $cartDetailsProduct = $cart->details()->where('product_id', $product_id);
        if ($cartDetailsProduct->count() === 0) {
            $newCart = $cart->details()->create([
                'product_id' => $product_id,
                'price' => $product->price,
                'qty' => $qty
            ]);
        } else {
            $newCart = $cartDetailsProduct->first();
            $newCart->qty += $qty;
            $newCart->save();
        }

        if ($newCart) {
            return response()->json([
                'status' => 'ok',
                'message' => $cart->details()->get()
            ]);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'server error'
            ]);
        }
    }

    /**
     * show summary of cart
     */

    /**
     * get menampilkan dalam bentuk array, dan harus perulangan
     * jika menggunakan first cuman ngambil 1 record yg paling atas langsung mereturn berupa object kalo object bisa langsung ngambil idnya brp, creatednya brp
     */

    public function show()
    {
        $user = auth()->user();
        $cart = Cart::where('user_id', $user->id)->first();
        $cartDetails = $cart->details()->get();

        $result = [];
        /**
         * pake perulangan
         * yg proses php
         */
        foreach ($cartDetails as $key => $detail) {
            if (!isset($result[$detail->product_id])) {
                $result[$detail->product_id] = [
                    'product_id' => $detail->product_id,
                    'qty' => $detail->qty,
                    'price' => $detail->price,
                    'total' => $detail->qty * $detail->price,
                    'keterangan' => 'Silahkan Checkout'
                ];
            } else {
                $result[$detail->product_id]['qty'] += $detail->qty;
                $result[$detail->product_id]['total'] += $detail->qty * $detail->price;
            }
        }

        return $result;

        /**
         * pake group by
         * yg proses mysql
         */
        // $cartDetails = $cart->details()
        //     ->select('product_id', DB::raw('SUM(qty) as sumofqty, SUM(price*qty) as sumofprice'))
        //     ->groupBy('product_id')
        //     ->get();

        // return $cartDetails;
    }
}
