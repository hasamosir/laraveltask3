<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    use HasFactory;

    protected $fillable = [
        'status', 'date_start', 'date_end', 'min_trans_amount', 'discount'
    ];

    public function reserve()
    {
        return $this->belongsTo(Order::class);
    }
}
