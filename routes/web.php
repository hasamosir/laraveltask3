<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin/register', [App\Http\Controllers\UI\FormRegisterController::class, 'showForm']);
Route::post('/admin/register', [App\Http\Controllers\UI\FormRegisterController::class, 'register'])->name('admin.register.post');
Route::post('/admin/verify-otp', [App\Http\Controllers\UI\FormRegisterController::class, 'verify'])->name('admin.register.verify');


Route::get('/admin/login', [App\Http\Controllers\UI\LoginController::class, 'create']);
Route::post('/admin/login', [App\Http\Controllers\UI\LoginController::class, 'store'])->name('admin.store.post');
Route::post('/admin/verify-login', [App\Http\Controllers\UI\LoginController::class, 'verifyLogin'])->name('admin.login.verify');

// Route::get('/admin/login', [App\Http\Controllers\UI\AksesLoginController::class, 'create'])->name('login');
// Route::post('/admin/login', [App\Http\Controllers\UI\AksesLoginController::class, 'store'])->name('admin.store');

Auth::routes();

Route::get('/admin/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
