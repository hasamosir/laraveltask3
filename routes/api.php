<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::namespace('Api')->group(function () {
//     Route::post('/register', 'RegisterController@register');
//     Route::post('/verify-otp', 'RegisterController@verifyOtp');

//     Route::post('/login', 'LoginController@login');
// });

// Route::post('/register', [\App\Http\Controllers\Api\RegisterController::class, 'register']);
// Route::post('/verify-otp', [\App\Http\Controllers\Api\RegisterController::class, 'verifyOtp']);



Route::post('/register', 'Api\RegisterController@register');
Route::post('/verify-otp', 'Api\RegisterController@verifyOtp');

Route::post('/login', 'Api\LoginController@login');
Route::post('/verify-login', 'Api\LoginController@verifyOtp');

Route::get('/product-login', 'Api\LoginController@product');
Route::get('/product', 'Api\ProductController@index');
Route::post('/img-product', 'Api\ProductController@store');

Route::middleware('auth:sanctum')->post('/checkout', 'Api\BookedController@checkout');
Route::middleware('auth:sanctum')->get('/summary', 'Api\BookedController@show');

Route::middleware('auth:sanctum')->post('/order', 'Api\OrderController@order');
Route::middleware('auth:sanctum')->get('/details', 'Api\OrderController@detail');

Route::post('/voucher', 'Api\VoucherController@voucher');
Route::middleware('auth:sanctum')->get('/details', 'Api\VoucherController@detail');

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
