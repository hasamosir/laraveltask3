@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card">
                <div class="card-header text-white bg-primary">Verifikasi</div>
                <div class="card-body">
                    {{$response->message}}
                </div>
                <a href="{{route('home')}}" type="button" class="btn btn-primary" style="float: right">Back</a>
            </div>
        </div>
    </div>
</div>
@endsection