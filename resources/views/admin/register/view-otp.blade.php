@extends('layouts.app')

@section('content')
<div class="container">
    {{$response->data->otp ?? '' }}
    <div class="row justify-content-center" data-otp="{{$response->data->otp ?? '' }}">
        <div class="col-12">
            <div class="card">
                <div class="card-header text-white bg-primary">Register</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('admin.register.verify') }}">
                        @csrf
                        <div class="mb-3 row">
                            <label class="col-sm-2 col-form-label">OTP</label>
                            <div class="col-sm-10">
                                <input type="hidden" class="form-control" name="phone" value="{{$response->data->phone ?? '' }}">
                                <input type="text" class="form-control" name="otp">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection